https://qooba.net/2018/12/01/boosting-elasticsearch-with-machine-learning-elasticsearch-ranklib-docker/

writeToFile()

function writeToFile() {
    var objects = heap.objects("char[]", false);
    var FileClass=Java.type("java.io.File");
    var FileWriterClass=Java.type("java.io.FileWriter");
    var writer = new FileWriterClass(new FileClass("mytextfile.txt"));

    while (objects.hasMoreElements()) {
        var busObj = objects.nextElement();
	if (busObj.toString().indexOf("CPU") > 0) {
        	writer.write("Query: "+busObj.toString()+"\n");
	}
    }
    writer.close();
    return { Result: "done" };
}